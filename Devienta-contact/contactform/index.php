<!doctype html>
<html lang="en">
  <head>
  	<title>Contact Form 10</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/style.css">

	</head>
	<body>
	<section class="ftco-section img bg-hero" style="background-image: url(images/bg_3.jpg);">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h1 class="heading-section">CONTACT FORM</h1>
					<h2 class="heading-section">'Deyranama.store'</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-11">
					<div class="wrapper">
						<div class="row no-gutters justify-content-between">
							<div class="col-lg-6 d-flex align-items-stretch">
								<div class="info-wrap w-100 p-5">
									<h3 class="mb-4">Contact us</h3>
				        	<div class="dbox w-100 d-flex align-items-start">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-map-marker"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Alamat:</span> Jl.Agung no 46 Rt01/Rw04 Dusun Sawahan, Genteng Kulon, Genteng, Banyuwangi, JATIM</p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-phone"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Telepon:</span> <a href="tel://1234567920">+62822 1234 5678</a></p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-paper-plane"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Email:</span> <a href="mailto:info@yoursite.com">deyranamastore@gmail.com</a></p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-globe"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Website</span> <a href="#">http://deyranamastore.com</a></p>
					          </div>
				          </div>
			          </div>
							</div>
							<div class="col-lg-5">
								<div class="contact-wrap w-100 p-md-5 p-4">
									<h3 class="mb-4">Get in touch</h3>
									<div id="form-message-warning" class="mb-4"></div> 
				      		<div id="form-message-success" class="mb-4">
				            Your message was sent, thank you!
				      		</div>
									<form method="POST" id="contactForm" name="contactForm">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<input type="text" class="form-control" name="name" id="name" placeholder="Name">
												</div>
											</div>
											<div class="col-md-12"> 
												<div class="form-group">
													<input type="email" class="form-control" name="email" id="email" placeholder="Email">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<textarea name="message" class="form-control" id="message" cols="30" rows="5" placeholder="Message"></textarea>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input type="submit" value="Send Message" class="btn btn-primary">
													<div class="submitting"></div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h2 style="color: #fff">------------------------------------------------------------------------------------------------------------------------------</h2>
		<table class="table table-striped" style="color: #fff">
                    <thead class="thead-dark">
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Id</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        include "connect.php"; //memanggil file connect.php
                        $query_mysql = mysqli_query($conn, "select * from tb_contactform")or die(mysqli_error($conn)); //mengambil isi database
                        $nomor = 1; 
                        while($data = mysqli_fetch_array($query_mysql)){ //memecah database menjadi field yang ada di dalamnya
                    ?>
                    <tr>
                      <th scope="row"><?php echo $nomor++ ?></th>
                      <td><?php echo $data['id']?></td> <!-- menampilkan isi field -->
                      <td><?php echo $data['nama']?></td> <!-- menampilkan isi field -->
                      <td><?php echo $data['email']?></td> <!-- menampilkan isi field -->
                      <td><?php echo $data['phone']?></td> <!-- menampilkan isi field -->
                    </tr>
                    <?php 
                        }
                    ?>
                    </tbody>
                </table>
				<?php
					$apiKey = "e89d2f6e9267159e060934c971785140";
					$cityId = "1650077";
					$googleApiUrl = "http://api.openweathermap.org/data/2.5/weather?id=" . $cityId . "&lang=en&units=metric&APPID=" . $apiKey;

					$ch = curl_init();

					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					$response = curl_exec($ch);

					curl_close($ch);
					$data = json_decode($response);
					$currentTime = time();
				?>
				<div class="report-container" style="color: #fff;">
					<h2 style="color: #fff">------------------------------------------------------------------------------------------------------------------------------</h2>
			        <center><h2 style="color: #fff"><?php echo $data->name; ?> Weather Status</h2></center>
			        <center>
			        <div class="time">
			            <div><?php echo date("l g:i a", $currentTime); ?></div>
			            <div><?php echo date("jS F, Y",$currentTime); ?></div>
			            <div><?php echo ucwords($data->weather[0]->description); ?></div>
			        </div>
			        <div class="weather-forecast">
			            <img
			                src="http://openweathermap.org/img/w/<?php echo $data->weather[0]->icon; ?>.png"
			                class="weather-icon" /> <?php echo $data->main->temp_max; ?>°C<span
			                class="min-temperature"><?php echo $data->main->temp_min; ?>°C</span>
			        </div>
			        <div class="time">
			            <div>Humidity: <?php echo $data->main->humidity; ?> %</div>
			            <div>Wind: <?php echo $data->wind->speed; ?> km/h</div>
			        </div>
			        </center>
    			</div>
	</section>

	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/main.js"></script>

	</body>
</html>

